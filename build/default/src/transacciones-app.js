import{PolymerElement,html}from"./my-app.js";class TransaccionesApp extends PolymerElement{static get template(){return html`
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style include="shared-styles">
      :host {
        display: block;
        padding: 10px;
      }
      .content-loading{
        margin-left: 80px;
        margin-top: 30px;
      }
      .btn-add{
        color: #fff;
	      text-decoration: none;
      }
    </style>
      <iron-ajax
        id="transaccionesAjax"
        auto
        url="http://localhost:3052/Transacciones"
        method="get"
        handle-as="json"
        content-type="application/json"
        on-response="respuestaRecibida"
        on-error="errorRecibido"
        loading="{{cargando}}"
        last-response="{{transacciones}}">
      </iron-ajax>

      <div class="row">
      <div class="col-md-4">
        <iron-selector selected="[[page]]" attr-for-selected="name" class="drawer-list" role="navigation">
          <paper-button raised class="primary" >
          <a class="btn-add" name="transaccion-add" href="[[rootPath]]transaccion-add">
          Agregar Transacción</a>
          </paper-button>
        </iron-selector>
      </div>
      <div class="col-md-4">
        <paper-button raised class="btnrefresh success" on-tap="getTransacciones">Refrescar</paper-button>
      </div>
      </div>
      <div class="row">
      <div class="content-loading" hidden$="[[!cargando]]">
        <paper-spinner alt="Cargando las transacciones..." active="[[cargando]]"></paper-spinner>
        <div hidden$="[[!cargando]]">Cargando</div>
      </div>
      </div>
      <div class="row">
      <div class="card" hidden$="[[cargando]]">
        <h1>Transacciones</h1>
        <div class="row">
          <br>
          <div class="table-responsive">
            <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th scope="col">Fecha</th>
                <th scope="col">Cuenta</th>
                <th scope="col"># Mov.</th>
                <th scope="col">Concepto</th>
                <th scope="col">Descripción</th>
                <th scope="col">Cliente</th>
                <th scope="col">Grupo</th>
                <th scope="col">Importe</th>
                <th scope="col">Moneda</th>
              </tr>
            </thead>
            <tbody>
            <template is="dom-repeat" items="[[transacciones]]">
              <tr>
                <td scope="row">[[item.fh_operacion]]</td>
                <td>[[item.cuenta]]</td>
                <td>[[item.cod_mov_cta]]</td>
                <td>[[item.cod_concep]]</td>
                <td>[[item.des_observa]]</td>
                <td>[[item.cliente]]</td>
                <td>[[item.grupo]]</td>
                <td>[[item.imp_trans]]</td>
                <td>[[item.moneda]]</td>
              </tr>
            </template>
            </tbody>
          </table>
          </div>
          </div>
        </div>
      </div>
    `}getTransacciones(){this.$.transaccionesAjax.generateRequest()}respuestaRecibida(e,request){if(request.succeeded){this.mensaje="la solicitud se resolvi\xF3 correctamente con c\xF3digo "+request.status}else{this.mensaje="la solicitud nos ofreci\xF3 resultados incorrectos, con c\xF3digo "+request.status}console.log(this.mensaje)}errorRecibido(e,error){console.log(error.request.status);this.mensaje="Error en la solicitud, con c\xF3digo "+error.request.status;console.log(this.mensaje)}}window.customElements.define("transacciones-app",TransaccionesApp);