import{PolymerElement,html}from"./my-app.js";class UsuariosApp extends PolymerElement{static get template(){return html`
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style include="shared-styles">
      :host {
        display: block;
        padding: 10px;
      }
      .content-loading{
        margin-left: 80px;
        margin-top: 30px;
      }
      .btn-add{
        color: #fff;
	      text-decoration: none;
      }
    </style>
      <iron-ajax
        id="usuariosAjax"
        auto
        url="http://localhost:3052/Usuarios"
        method="get"
        handle-as="json"
        content-type="application/json"
        on-response="respuestaRecibida"
        on-error="errorRecibido"
        loading="{{cargando}}"
        last-response="{{usuarios}}">
      </iron-ajax>

      <div class="row">
        <div class="col-md-4">
          <iron-selector selected="[[page]]" attr-for-selected="name" class="drawer-list" role="navigation">
            <paper-button raised class="primary" >
            <a class="btn-add" name="usuario-add" href="[[rootPath]]usuario-add">
            Agregar Usuario</a>
            </paper-button>
          </iron-selector>
        </div>
        <div class="col-md-4">
          <paper-button raised class="btnrefresh success" on-tap="getUsuarios">Refrescar</paper-button>
        </div>
      </div>
      <div class="row">
        <div class="content-loading" hidden$="[[!cargando]]">
          <paper-spinner alt="Cargando los usuarios..." active="[[cargando]]"></paper-spinner>
          <div hidden$="[[!cargando]]">Cargando</div>
        </div>
      </div>
      <div class="row">
        <div class="card" hidden$="[[cargando]]">
          <h1>Usuarios</h1>
          <div class="row">
            <br>
            <div class="table-responsive">
              <table class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellidos</th>
                    <th scope="col">Email</th>
                    <th scope="col">Password</th>
                  </tr>
                </thead>
              <tbody>
                <template is="dom-repeat" items="[[usuarios]]">
                  <tr>
                    <td scope="row">[[item.idcliente]]</td>
                    <td>[[item.nombre]]</td>
                    <td>[[item.apellidos]]</td>
                    <td>[[item.email]]</td>
                    <td>[[item.password]]</td>
                  </tr>
                </template>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    `}getUsuarios(){this.$.usuariosAjax.generateRequest()}respuestaRecibida(e,request){if(request.succeeded){this.mensaje="la solicitud se resolvi\xF3 correctamente con c\xF3digo "+request.status}else{this.mensaje="la solicitud nos ofreci\xF3 resultados incorrectos, con c\xF3digo "+request.status}console.log(this.mensaje)}errorRecibido(e,error){console.log(error.request.status);this.mensaje="Error en la solicitud, con c\xF3digo "+error.request.status;console.log(this.mensaje)}}window.customElements.define("usuarios-app",UsuariosApp);