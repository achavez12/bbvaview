import{Base,Polymer,PolymerElement,html}from"./my-app.js";Polymer({is:"iron-localstorage",properties:{/**
     * localStorage item key
     */name:{type:String,value:""},/**
     * The data associated with this storage.
     * If set to null item will be deleted.
     * @type {*}
     */value:{type:Object,notify:!0},/**
     * If true: do not convert value to JSON on save/load
     */useRaw:{type:Boolean,value:!1},/**
     * Value will not be saved automatically if true. You'll have to do it
     * manually with `save()`
     */autoSaveDisabled:{type:Boolean,value:!1},/**
     * Last error encountered while saving/loading items
     */errorMessage:{type:String,notify:!0},/** True if value has been loaded */_loaded:{type:Boolean,value:!1}},observers:["_debounceReload(name,useRaw)","_trySaveValue(autoSaveDisabled)","_trySaveValue(value.*)"],ready:function(){this._boundHandleStorage=this._handleStorage.bind(this)},attached:function(){window.addEventListener("storage",this._boundHandleStorage)},detached:function(){window.removeEventListener("storage",this._boundHandleStorage)},_handleStorage:function(ev){if(ev.key==this.name){this._load(!0)}},_trySaveValue:function(){if(this.autoSaveDisabled===void 0||this._doNotSave){return}if(this._loaded&&!this.autoSaveDisabled){this.debounce("save",this.save)}},_debounceReload:function(){if(this.name!==void 0&&this.useRaw!==void 0){this.debounce("reload",this.reload)}},/**
   * Loads the value again. Use if you modify
   * localStorage using DOM calls, and want to
   * keep this element in sync.
   */reload:function(){this._loaded=!1;this._load()},/**
   * loads value from local storage
   * @param {boolean=} externalChange true if loading changes from a different window
   */_load:function(externalChange){try{var v=window.localStorage.getItem(this.name)}catch(ex){this.errorMessage=ex.message;this._error("Could not save to localStorage.  Try enabling cookies for this page.",ex)};if(null===v){this._loaded=!0;this._doNotSave=!0;// guard for save watchers
this.value=null;this._doNotSave=!1;this.fire("iron-localstorage-load-empty",{externalChange:externalChange},{composed:!0})}else{if(!this.useRaw){try{// parse value as JSON
v=JSON.parse(v)}catch(x){this.errorMessage="Could not parse local storage value";Base._error("could not parse local storage value",v);v=null}}this._loaded=!0;this._doNotSave=!0;this.value=v;this._doNotSave=!1;this.fire("iron-localstorage-load",{externalChange:externalChange},{composed:!0})}},/**
   * Saves the value to localStorage. Call to save if autoSaveDisabled is set.
   * If `value` is null or undefined, deletes localStorage.
   */save:function(){var v=this.useRaw?this.value:JSON.stringify(this.value);try{if(null===this.value||this.value===void 0){window.localStorage.removeItem(this.name)}else{window.localStorage.setItem(this.name,/** @type {string} */v)}}catch(ex){// Happens in Safari incognito mode,
this.errorMessage=ex.message;Base._error("Could not save to localStorage. Incognito mode may be blocking this action",ex)}}/**
     * Fired when value loads from localStorage.
     *
     * @event iron-localstorage-load
     * @param {{externalChange:boolean}} detail -
     *     externalChange: true if change occured in different window.
     */ /**
         * Fired when loaded value does not exist.
         * Event handler can be used to initialize default value.
         *
         * @event iron-localstorage-load-empty
         * @param {{externalChange:boolean}} detail -
         *     externalChange: true if change occured in different window.
         */});class LoginApp extends PolymerElement{static get template(){return html`
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <style include="shared-styles">
        :host {
          display: block;
        }

        body {
        	background: #1d7bc1;
        }

        .login-box{
        	margin-right: auto;
        	margin-left: auto;
        	margin-top: 50px;
          padding-bottom: 10px;
        	background: #fff;
        	width: 50%;

        	box-shadow: 3px 5px 6px #333;

        }

        .img-login{
        	margin-right: 15px;
        }

        .title-login{
        	margin-top: 2px;
        	font-weight: bold;
        	color: #fff;
        	text-decoration: none;
        }
        .login-header{
        	padding: 10px;
        	background: #007bff;
        }
        .login-header a{
        	text-decoration: none;
        }
        .login-body{
        	padding: 10px;
        	border-bottom: 1px solid #007bff;
        }
        .login-footer{
        	padding: 10px;
        }

        @media screen and (max-width: 800px){
        	.login-box{
        		width: 100%;
        		margin-top: 10px;
        	}
        }


        .alert-error {
          background: #ffcdd2;
          border: 1px solid #f44336;
          border-radius: 3px;
          color: #333;
          font-size: 14px;
          padding: 10px;
        }

        .wrapper-btns {
          margin-top: 15px;
        }
        paper-button.link {
          color: #757575;
        }

      </style>

      <app-location route="{{route}}"></app-location>

      <iron-ajax
        id="loginAjax"
        url="http://localhost:3052/login"
        method="POST"
        handle-as="json"
        content-type="application/json"
        on-response="respuestaRecibida"
        on-error="errorRecibido"
        loading="{{cargando}}"
        last-response="{{logindata}}">
      </iron-ajax>

      <iron-localstorage name="view-storage"
          value="{{viewcliente}}" on-iron-localstorage-load-empty="initializeCliente">
      </iron-localstorage>

      <div class="container">
    		<div class="login-box" hidden$="[[viewcliente.cliente]]">
    			<div class="login-header d-flex justify-content-center">
    				<img class="img-login" src="images/logo_bbva.png" height="25" class="align-top" alt="">
    				<a href="#" class="title-login"><span>Login</span></a>
    			</div>
    			<form >
          <div class="container" hidden$="[[!response]]">
            <br>
            <div class="alert alert-info alert-dismissible fade show" role="alert">
              <strong>Bienvenido!</strong> [[logindata.nombre]] [[logindata.apellidos]].
            </div>
            <br>
          </div>
    			<div class="login-body" hidden$="[[response]]">
              <div class="container" hidden$="[[!valida]]">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <strong>Faltan Datos por Llenar!</strong>.
                </div>
              </div>
              <div class="container" hidden$="[[!nouser]]">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  <strong>Usuario No Reconocido!</strong>.
                </div>
              </div>
              <div class="container" hidden$="[[!errores]]">
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                  <strong>Error!</strong> No se pudo realizar la operación.
                </div>
              </div>
    				  <div class="form-group">
    				    <label for="exampleInputEmail1">Email address</label>
    				    <input type="email" class="form-control" value="{{email::input}}" aria-describedby="emailHelp" placeholder="Enter email">
    				    <small id="emailHelp" class="form-text text-muted">Nunca compartas tus credenciales.</small>
    				  </div>
    				  <div class="form-group">
    				    <label for="exampleInputPassword1">Password</label>
    				    <input type="password" class="form-control" value="{{password::input}}" placeholder="Password">
    				  </div>
    			</div>
    			<div class="login-footer" hidden$="[[response]]">
    				 <button type="button" class="btn btn-primary btn-block" on-click="login">Iniciar</button>
    			</div>
    			</form>
    		</div>
        <div class="login-box" hidden$="[[!viewcliente.cliente]]">
    			<div class="login-header d-flex justify-content-center">
    				<img class="img-login" src="images/logo_bbva.png" height="25" class="align-top" alt="">
    				<a href="#" class="title-login"><span>Login</span></a>
    			</div>
    			<form >
          <div class="container">
            <br>
            <div class="alert alert-info alert-dismissible fade show" role="alert">
              <strong>Bienvenido!</strong> [[viewcliente.nombre]] [[viewcliente.apellidos]].
            </div>

            <div class="row">
            <div class="col-md-4">

            </div>
            <div class="col-md-4">
              <paper-button raised class="btnrefresh success" on-tap="logOut">Salir</paper-button>
            </div>
              <div class="col-md-4">

              </div>
            </div>

          </div>
    			</form>
    		</div>
    	</div>

    `}static get properties(){return{page:{type:String,value:"Login"},email:{type:String},password:{type:String},response:{type:Boolean,value:!1},valida:{type:Boolean,value:!1},errores:{type:Boolean,value:!1},nouser:{type:Boolean,value:!1},viewcliente:{type:Object}}}login(){console.log("Logeando");console.log("Email: "+this.email);console.log("Password: "+this.password);if(""==this.email||""==this.password||"undefined"===typeof this.email||"undefined"===typeof this.password){this.valida=!0;this.nouser=!1}else{var obj={email:this.email,password:this.password};this.$.loginAjax.body=obj;this.$.loginAjax.generateRequest()}}respuestaRecibida(e,request){if(request.succeeded){this.mensaje="la solicitud se resolvi\xF3 correctamente con c\xF3digo "+request.status;if(null==this.logindata){this.nouser=!0;this.response=!1;this.errores=!1;this.valida=!1}else{this.nouser=!1;this.response=!0;this.errores=!1;this.valida=!1;this.email="";this.password="";this.viewcliente={idcliente:this.logindata.idcliente,nombre:this.logindata.nombre,apellidos:this.logindata.apellidos,cliente:!0};location.reload()}}else{this.mensaje="la solicitud nos ofreci\xF3 resultados incorrectos, con c\xF3digo "+request.status}console.log(this.mensaje);console.log(this.logindata)}errorRecibido(e,error){console.log(error.request.status);this.mensaje="Error en la solicitud, con c\xF3digo "+error.request.status;console.log(this.mensaje);this.errores=!0}initializeCliente(){this.viewcliente={cliente:!1}}logOut(){this.viewcliente={cliente:!1};location.reload()}}window.customElements.define("login-app",LoginApp);