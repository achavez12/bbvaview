import{PolymerElement,html}from"./my-app.js";class UsuarioAdd extends PolymerElement{static get template(){return html`
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style include="shared-styles">
      :host {
        display: block;

        padding: 10px;
      }
      .btn-back{
        color: #fff;
	      text-decoration: none;
      }
      .btnrefresh{
        margin-left: 20px;
      }

    </style>

    <iron-ajax
      id="addUsuarioAjax"
      url="http://localhost:3052/usuario"
      method="POST"
      handle-as="json"
      content-type="application/json"
      on-response="respuestaRecibida"
      on-error="errorRecibido">
    </iron-ajax>

      <div class="card" hidden$="[[response]]">
        <h3>Nuevo Usuario</h3>
          <div clas="col-md-12">
            <div class="form-group">
              <label for="lblId">ID</label>
              <input type="text" class="form-control" placeholder="ID" value="{{id::input}}">
            </div>
            <div class="form-group">
  				    <label for="lblNombre">Nombre</label>
  				    <input type="text" class="form-control" placeholder="Nombre" value="{{nombre::input}}">
  				  </div>
            <div class="form-group">
  				    <label for="lblApellidos">Apellidos</label>
  				    <input type="text" class="form-control" placeholder="Apellidos" value="{{apellidos::input}}">
  				  </div>
            <div class="form-group">
  				    <label for="lblEmail">Email</label>
  				    <input type="email" class="form-control" placeholder="Email" value="{{email::input}}">
  				  </div>
  				  <div class="form-group">
  				    <label for="lblPassword">Password</label>
  				    <input type="password" class="form-control" placeholder="Password" value="{{password::input}}">
  				  </div>
          </div>
        </div>

        <div class="container" hidden$="[[!valida]]">
          <br>
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Faltan Datos por Llenar!</strong>.
          </div>
        </div>

        <div class="container" hidden$="[[!errores]]">
          <br>
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Error!</strong> No se pudo realizar la operación.
          </div>
        </div>

      <div class="container" hidden$="[[response]]">
        <paper-button raised class="primary" on-tap="addUsuario">Guardar</paper-button>
      </div>

      <div class="container" hidden$="[[!response]]">
        <br>
        <iron-selector selected="[[page]]" attr-for-selected="name" class="drawer-list" role="navigation">
          <paper-button raised class="primary" >
          <a class="btn-back" name="usuarios-app" href="[[rootPath]]usuarios-app">
          Regresar</a>
          </paper-button>
        </iron-selector>

        <paper-button raised class="btnrefresh primary" on-tap="recargar">Nuevo</paper-button>

        <br>
        <br>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
				  <strong>Usuario Guardado!</strong> [[mensaje]].
				</div>
      </div>


    `}static get properties(){return{id:{type:String},nombre:{type:String},apellidos:{type:String},email:{type:String},password:{type:String},response:{type:Boolean,value:!1},valida:{type:Boolean,value:!1},errores:{type:Boolean,value:!1}}}addUsuario(){console.log("Agregando Usuario");console.log("Id: "+this.id);console.log("Nombre: "+this.nombre);console.log("Apellidos: "+this.apellidos);console.log("Email: "+this.email);console.log("Password: "+this.password);if(""==this.id||""==this.nombre||""==this.apellidos||""==this.email||""==this.password||"undefined"===typeof this.id||"undefined"===typeof this.nombre||"undefined"===typeof this.apellidos||"undefined"===typeof this.email||"undefined"===typeof this.password){this.valida=!0}else{var obj={idcliente:this.id,nombre:this.nombre,apellidos:this.apellidos,email:this.email,password:this.password};this.$.addUsuarioAjax.body=obj;this.$.addUsuarioAjax.generateRequest();this.valida=!1}}recargar(){this.response=!1;this.errores=!1}respuestaRecibida(e,request){if(request.succeeded){this.mensaje="la solicitud se resolvi\xF3 correctamente con c\xF3digo "+request.status}else{this.mensaje="la solicitud nos ofreci\xF3 resultados incorrectos, con c\xF3digo "+request.status}this.response=!0;this.errores=!1;this.id="";this.nombre="";this.apellidos="";this.email="";this.password="";console.log(this.mensaje)}errorRecibido(e,error){console.log(error.request.status);this.mensaje="Error en la solicitud, con c\xF3digo "+error.request.status;console.log(this.mensaje);this.errores=!0}}window.customElements.define("usuario-add",UsuarioAdd);