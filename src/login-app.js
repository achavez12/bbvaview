import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-localstorage/iron-localstorage.js';
import '@polymer/iron-input/iron-input.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/app-route/app-location.js';

class LoginApp extends PolymerElement {
  static get template() {
    return html`
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <style include="shared-styles">
        :host {
          display: block;
        }

        body {
        	background: #1d7bc1;
        }

        .login-box{
        	margin-right: auto;
        	margin-left: auto;
        	margin-top: 50px;
          padding-bottom: 10px;
        	background: #fff;
        	width: 50%;

        	box-shadow: 3px 5px 6px #333;

        }

        .img-login{
        	margin-right: 15px;
        }

        .title-login{
        	margin-top: 2px;
        	font-weight: bold;
        	color: #fff;
        	text-decoration: none;
        }
        .login-header{
        	padding: 10px;
        	background: #007bff;
        }
        .login-header a{
        	text-decoration: none;
        }
        .login-body{
        	padding: 10px;
        	border-bottom: 1px solid #007bff;
        }
        .login-footer{
        	padding: 10px;
        }

        @media screen and (max-width: 800px){
        	.login-box{
        		width: 100%;
        		margin-top: 10px;
        	}
        }


        .alert-error {
          background: #ffcdd2;
          border: 1px solid #f44336;
          border-radius: 3px;
          color: #333;
          font-size: 14px;
          padding: 10px;
        }

        .wrapper-btns {
          margin-top: 15px;
        }
        paper-button.link {
          color: #757575;
        }

      </style>

      <app-location route="{{route}}"></app-location>

      <iron-ajax
        id="loginAjax"
        url="http://localhost:3052/login"
        method="POST"
        handle-as="json"
        content-type="application/json"
        on-response="respuestaRecibida"
        on-error="errorRecibido"
        loading="{{cargando}}"
        last-response="{{logindata}}">
      </iron-ajax>

      <iron-localstorage name="view-storage"
          value="{{viewcliente}}" on-iron-localstorage-load-empty="initializeCliente">
      </iron-localstorage>

      <div class="container">
    		<div class="login-box" hidden$="[[viewcliente.cliente]]">
    			<div class="login-header d-flex justify-content-center">
    				<img class="img-login" src="images/logo_bbva.png" height="25" class="align-top" alt="">
    				<a href="#" class="title-login"><span>Login</span></a>
    			</div>
    			<form >
          <div class="container" hidden$="[[!response]]">
            <br>
            <div class="alert alert-info alert-dismissible fade show" role="alert">
              <strong>Bienvenido!</strong> [[logindata.nombre]] [[logindata.apellidos]].
            </div>
            <br>
          </div>
    			<div class="login-body" hidden$="[[response]]">
              <div class="container" hidden$="[[!valida]]">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <strong>Faltan Datos por Llenar!</strong>.
                </div>
              </div>
              <div class="container" hidden$="[[!nouser]]">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  <strong>Usuario No Reconocido!</strong>.
                </div>
              </div>
              <div class="container" hidden$="[[!errores]]">
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                  <strong>Error!</strong> No se pudo realizar la operación.
                </div>
              </div>
    				  <div class="form-group">
    				    <label for="exampleInputEmail1">Email address</label>
    				    <input type="email" class="form-control" value="{{email::input}}" aria-describedby="emailHelp" placeholder="Enter email">
    				    <small id="emailHelp" class="form-text text-muted">Nunca compartas tus credenciales.</small>
    				  </div>
    				  <div class="form-group">
    				    <label for="exampleInputPassword1">Password</label>
    				    <input type="password" class="form-control" value="{{password::input}}" placeholder="Password">
    				  </div>
    			</div>
    			<div class="login-footer" hidden$="[[response]]">
    				 <button type="button" class="btn btn-primary btn-block" on-click="login">Iniciar</button>
    			</div>
    			</form>
    		</div>
        <div class="login-box" hidden$="[[!viewcliente.cliente]]">
    			<div class="login-header d-flex justify-content-center">
    				<img class="img-login" src="images/logo_bbva.png" height="25" class="align-top" alt="">
    				<a href="#" class="title-login"><span>Login</span></a>
    			</div>
    			<form >
          <div class="container">
            <br>
            <div class="alert alert-info alert-dismissible fade show" role="alert">
              <strong>Bienvenido!</strong> [[viewcliente.nombre]] [[viewcliente.apellidos]].
            </div>

            <div class="row">
            <div class="col-md-4">

            </div>
            <div class="col-md-4">
              <paper-button raised class="btnrefresh success" on-tap="logOut">Salir</paper-button>
            </div>
              <div class="col-md-4">

              </div>
            </div>

          </div>
    			</form>
    		</div>
    	</div>

    `;
  }
  static get properties() {
    return {
      page: {
        type: String,
        value: 'Login'
      },
      email:{
        type: String
      },
      password:{
        type: String
      },
      response:{
        type: Boolean,
        value: false
      },
      valida:{
        type: Boolean,
        value: false
      },
      errores:{
        type: Boolean,
        value: false
      },
      nouser:{
        type: Boolean,
        value: false
      },
      viewcliente:{
         type: Object
      }
    };
  }

  login(){
    console.log("Logeando");
    console.log("Email: "+this.email);
    console.log("Password: "+this.password);

    if(this.email == "" || this.password == ""
    || typeof this.email === 'undefined' || typeof this.password === 'undefined'){
      this.valida=true;
      this.nouser = false;
    }else{
      var obj = {
        email: this.email,
        password: this.password
      };
      this.$.loginAjax.body = obj;

      this.$.loginAjax.generateRequest();
    }

  }

  respuestaRecibida(e, request) {
    if(request.succeeded) {
      this.mensaje = 'la solicitud se resolvió correctamente con código ' + request.status
      if(this.logindata == null){
        this.nouser = true;
        this.response=false;
        this.errores=false;
        this.valida=false;
      }else{
        this.nouser = false;
        this.response=true;
        this.errores=false;
        this.valida=false;
        this.email="";
        this.password="";
        this.viewcliente = {
          idcliente: this.logindata.idcliente,
          nombre: this.logindata.nombre,
          apellidos: this.logindata.apellidos,
          cliente: true
        }
        location.reload();
      }
    } else {
      this.mensaje = 'la solicitud nos ofreció resultados incorrectos, con código ' + request.status
    }

    console.log(this.mensaje);
    console.log(this.logindata);
  }

  errorRecibido(e, error) {
    console.log(error.request.status);
    this.mensaje = 'Error en la solicitud, con código ' + error.request.status;
    console.log(this.mensaje);
    this.errores=true;
  }

  initializeCliente() {
    this.viewcliente = {
      cliente: false
    }
  }
  logOut() {
    this.viewcliente = {
      cliente: false
    }
    location.reload();
  }

}

window.customElements.define('login-app', LoginApp);
