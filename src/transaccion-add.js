import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/iron-input/iron-input.js';

class TransaccionAdd extends PolymerElement {
  static get template() {
    return html`
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style include="shared-styles">
      :host {
        display: block;

        padding: 10px;
      }
      .btn-back{
        color: #fff;
	      text-decoration: none;
      }
      .btnrefresh{
        margin-left: 20px;
      }

    </style>

    <iron-ajax
      id="addTransaccionAjax"
      url="http://localhost:3052/transaccion"
      method="POST"
      handle-as="json"
      content-type="application/json"
      on-response="respuestaRecibida"
      on-error="errorRecibido">
    </iron-ajax>

      <div class="card" hidden$="[[response]]">
        <h3>Nueva Transacción</h3>
          <div clas="col-md-12">
            <div class="form-group">
              <label for="lblFecha">Fecha Operación</label>
              <input type="date" class="form-control" placeholder="dd/mm/aaaa" value="{{fh_operacion::input}}">
            </div>
            <div class="form-group">
  				    <label for="lblCuenta">Cuenta</label>
  				    <input type="text" class="form-control" placeholder="Cuenta" value="{{cuenta::input}}">
  				  </div>
            <div class="form-group">
  				    <label for="lblCodMovCta"># Movimiento de Cuenta</label>
  				    <input type="text" class="form-control" placeholder="# de Movimiento" value="{{cod_mov_cta::input}}">
  				  </div>
            <div class="form-group">
  				    <label for="lblConcepto">Concepto</label>
  				    <input type="text" class="form-control" placeholder="Concepto" value="{{cod_concep::input}}">
  				  </div>
  				  <div class="form-group">
  				    <label for="lblObservacion">Descripción de Observación</label>
  				    <input type="text" class="form-control" placeholder="Descripción" value="{{des_observa::input}}">
  				  </div>
            <div class="form-group">
  				    <label for="lblCliente">Cliente</label>
  				    <input type="text" class="form-control" placeholder="Cliente" value="{{cliente::input}}">
  				  </div>
            <div class="form-group">
  				    <label for="lblGrupo">Grupo</label>
  				    <input type="text" class="form-control" placeholder="Grupo" value="{{grupo::input}}">
  				  </div>
            <div class="form-group">
            <label for="lblImporte">Importe de Transacción</label>
  				    <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">$</span>
                </div>
                <input type="text" class="form-control" placeholder="0" aria-label="Importe" value="{{imp_trans::input}}" aria-describedby="basic-addon1">
              </div>
  				  </div>
            <div class="form-group">
  				    <label for="lblMoneda">Moneda</label>
  				    <input type="text" class="form-control" placeholder="Moneda" value="{{moneda::input}}">
  				  </div>
          </div>
        </div>

        <div class="container" hidden$="[[!valida]]">
          <br>
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Faltan Datos por Llenar!</strong>.
          </div>
        </div>

        <div class="container" hidden$="[[!errores]]">
          <br>
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Error!</strong> No se pudo realizar la operación.
          </div>
        </div>

      <div class="container" hidden$="[[response]]">
        <paper-button raised class="primary" on-tap="addTransaccion">Guardar</paper-button>
      </div>

      <div class="container" hidden$="[[!response]]">
        <br>
        <iron-selector selected="[[page]]" attr-for-selected="name" class="drawer-list" role="navigation">
          <paper-button raised class="primary" >
          <a class="btn-back" name="transacciones-app" href="[[rootPath]]transacciones-app">
          Regresar</a>
          </paper-button>
        </iron-selector>

        <paper-button raised class="btnrefresh primary" on-tap="recargar">Nuevo</paper-button>

        <br>
        <br>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
				  <strong>Transacción Guardada!</strong> [[mensaje]].
				</div>
      </div>


    `;
  }

  static get properties(){
    return{
      fh_operacion:{
        type: String
      },
      cuenta:{
        type: String
      },
      cod_mov_cta:{
        type: String
      },
      cod_concep:{
        type: String
      },
      des_observa:{
        type: String
      },
      cliente:{
        type: String
      },
      grupo:{
        type: String
      },
      imp_trans:{
        type: String
      },
      moneda:{
        type: String
      },
      response:{
        type: Boolean,
        value: false
      },
      valida:{
        type: Boolean,
        value: false
      },
      errores:{
        type: Boolean,
        value: false
      }
    }
  }

  addTransaccion(){
    console.log("Agregando Usuario");
    console.log("Fecha: "+this.fh_operacion);
    console.log("Cuenta: "+this.cuenta);
    console.log("# Mov: "+this.cod_mov_cta);
    console.log("Concepto: "+this.cod_concep);
    console.log("Observa: "+this.des_observa);
    console.log("Cliente: "+this.cliente);
    console.log("Grupo: "+this.grupo);
    console.log("Importe: "+this.imp_trans);
    console.log("Moneda: "+this.moneda);

    if(this.fh_operacion == "" || this.cuenta == "" || this.cod_mov_cta == ""
    || this.cod_concep == "" || this.des_observa == "" || this.cliente == ""
    || this.grupo == "" || this.imp_trans == "" || this.moneda == ""
    || typeof this.fh_operacion === 'undefined' || typeof this.cuenta === 'undefined'
    || typeof this.cod_mov_cta === 'undefined' || typeof this.cod_concep === 'undefined'
    || typeof this.des_observa === 'undefined' || typeof this.cliente === 'undefined'
    || typeof this.grupo === 'undefined'
    || typeof this.imp_trans === 'undefined' || typeof this.moneda === 'undefined'){
      this.valida=true;
    }else{

      var obj = {
        fh_operacion: this.fh_operacion,
        cuenta: this.cuenta,
        cod_mov_cta: this.cod_mov_cta,
        cod_concep: this.cod_concep,
        des_observa: this.des_observa,
        cliente: this.cliente,
        grupo: this.grupo,
        imp_trans: this.imp_trans,
        moneda: this.moneda
      };
      this.$.addTransaccionAjax.body = obj;

      this.$.addTransaccionAjax.generateRequest();
      this.valida=false;
    }

  }

  recargar(){
    this.response=false;
    this.errores=false;
  }

  respuestaRecibida(e, request) {
    if(request.succeeded) {
      this.mensaje = 'la solicitud se resolvió correctamente con código ' + request.status
    } else {
      this.mensaje = 'la solicitud nos ofreció resultados incorrectos, con código ' + request.status
    }
    this.response=true;
    this.errores=false;

    this.fh_operacion="";
    this.cuenta="";
    this.cod_mov_cta="";
    this.cod_concep="";
    this.des_observa="";
    this.cliente="";
    this.grupo="";
    this.imp_trans="";
    this.moneda="";
    console.log(this.mensaje);
  }

  errorRecibido(e, error) {
    console.log(error.request.status);
    this.mensaje = 'Error en la solicitud, con código ' + error.request.status;
    console.log(this.mensaje);
    this.errores=true;
  }


}

window.customElements.define('transaccion-add', TransaccionAdd);
