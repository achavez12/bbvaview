import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/paper-button/paper-button.js';
import './my-icons.js';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(MyAppGlobals.rootPath);

class MyApp extends PolymerElement {
  static get template() {
    return html`
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <style>
        :host {
          --app-primary-color: #4285f4;
          --app-secondary-color: black;

          display: block;
        }

        app-drawer-layout:not([narrow]) [drawer-toggle] {
          display: none;
        }

        app-header {
          color: #fff;
          background-color: var(--app-primary-color);
        }

        app-header paper-icon-button {
          --paper-icon-button-ink-color: white;
        }

        .drawer-list {
          margin: 0 20px;
        }

        .drawer-list a {
          display: block;
          padding: 0 16px;
          text-decoration: none;
          color: var(--app-secondary-color);
          line-height: 40px;
        }

        .drawer-list a.iron-selected {
          color: black;
          font-weight: bold;
        }

        @media screen and (max-width: 800px){
          .img-logo{
        		position: relative;
        		height: 15px;
        	}
        }

        :root {
          --warning-color: #2692dd;
        }
        paper-button {
          color: #fff;
        }
        paper-button.warning {
          background: var(--warning-color);
        }

        .btn-add{
          color: #fff;
  	      text-decoration: none;
        }

      </style>

      <iron-localstorage name="view-storage"
          value="{{viewcliente}}" on-iron-localstorage-load-empty="initializeCliente">
      </iron-localstorage>

      <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location>

      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>

      <app-drawer-layout fullbleed="" narrow="{{narrow}}">
        <!-- Drawer content -->
        <app-drawer id="drawer" slot="drawer" swipe-open="[[narrow]]">
          <app-toolbar>Menu</app-toolbar>
          <div hidden$=[[!viewcliente.cliente]]>
          <iron-selector selected="[[page]]" attr-for-selected="name" class="drawer-list" role="navigation">
            <a name="transacciones-app" href="[[rootPath]]transacciones-app">Transacciones</a>
            <a name="usuarios-app" href="[[rootPath]]usuarios-app">Usuarios</a>
            <a name="categorizador-app" href="[[rootPath]]categorizador-app">Categorizador</a>
          </iron-selector>
          <div hidden$=[[!viewcliente.cliente]]>
            <iron-selector selected="[[page]]" attr-for-selected="name" class="drawer-list" role="navigation">
              <paper-button raised class="warning" >
              <a class="btn-add" name="usuario-add" href="[[rootPath]]login-app">
              Cerrar Sesión</a>
              </paper-button>
            </iron-selector>
          </div>
          </div>
        </app-drawer>

        <!-- Main content -->
        <app-header-layout has-scrolling-region="">

          <app-header slot="header" condenses="" reveals="" effects="waterfall">
            <app-toolbar>
              <paper-icon-button icon="my-icons:menu" drawer-toggle=""></paper-icon-button>
              <div main-title="">
                <img class="img-logo" src="images/logo_bbva.png"  height="25" class="align-top" alt="">
                <span>View Categorizer</span>
              </div>
            </app-toolbar>
          </app-header>

          <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
            <transacciones-app name="transacciones-app"></transacciones-app>
            <transaccion-add name="transaccion-add"></transaccion-add>
            <usuarios-app name="usuarios-app"></usuarios-app>
            <usuario-add name="usuario-add"></usuario-add>
            <my-view404 name="view404"></my-view404>
            <login-app name="login-app"></login-app>
            <categorizador-app name="categorizador-app"></categorizador-app>
          </iron-pages>
        </app-header-layout>
      </app-drawer-layout>
    `;
  }

  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      routeData: Object,
      subroute: Object,
      viewcliente:{
         type: Object
      }
    };
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  _routePageChanged(page) {
     // Show the corresponding page according to the route.
     //
     // If no page was found in the route data, page will be an empty string.
     // Show 'view1' in that case. And if the page doesn't exist, show 'view404'.
    if (!page) {
      this.page = 'login-app';
    } else if (['transacciones-app', 'usuarios-app', 'usuario-add','login-app','categorizador-app','view404','transaccion-add'].indexOf(page) !== -1) {
      this.page = page;
    } else {
      this.page = 'view404';
    }

    // Close a non-persistent drawer when the page & route are changed.
    if (!this.$.drawer.persistent) {
      this.$.drawer.close();
    }
  }

  _pageChanged(page) {
    // Import the page component on demand.
    //
    // Note: `polymer build` doesn't like string concatenation in the import
    // statement, so break it up.
    switch (page) {
      case 'transacciones-app':
        import('./transacciones-app.js');
        break;
      case 'usuarios-app':
        import('./usuarios-app.js');
        break;
      case 'view404':
        import('./my-view404.js');
        break;
      case 'login-app':
        import('./login-app.js');
        break;
      case 'categorizador-app':
        import('./categorizador-app.js');
        break;
      case 'usuario-add':
        import('./usuario-add.js');
        break;
      case 'transaccion-add':
        import('./transaccion-add.js');
        break;
    }
  }
}

window.customElements.define('my-app', MyApp);
