import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import '@polymer/paper-button/paper-button.js';

class CategorizadorApp extends PolymerElement {
  static get template() {
    return html`
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>
      <div class="container">
        <paper-button raised class="primary" on-tap="categorizar">Categorizar</paper-button>
      </div>
      <div class="card">
        <h1>Categorizador</h1>
        <div class="row">
          <br>
            <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">IdCliente</th>
                <th scope="col">Fecha</th>
                <th scope="col">IdOperacion</th>
                <th scope="col">Monto</th>
                <th scope="col">Moneda</th>
                <th scope="col">Concepto</th>
                <th scope="col">Categoria</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">D0152313</th>
                <td>2019-05-04</td>
                <td>qrqBwTxYKT</td>
                <td>791.7</td>
                <td>MXN</td>
                <td>GRUPO ELEKTRA SAB DE CV</td>
                <td><b>Servicios</b></td>
              </tr>
              <tr>
                <th scope="row">5D0153149</th>
                <td>2019-05-02</td>
                <td>UIwjRBoIcl</td>
                <td>64.33</td>
                <td>MXN</td>
                <td>Ralph Lauren</td>
                <td><b>Gastos personales</b></td>
              </tr>
              <tr>
                <th scope="row">7D0153150</th>
                <td>2019-05-03</td>
                <td>hgJtafGKVM</td>
                <td>253.28</td>
                <td>MXN</td>
                <td>MEGACABLE</td>
                <td><b>Entretenimiento</b></td>
              </tr>
              <tr>
                <th scope="row">4D0152363</th>
                <td>2019-05-04</td>
                <td>XrXbYEkqOo</td>
                <td>995.82</td>
                <td>MXN</td>
                <td>Kellogg’s</td>
                <td><b>Comida</b></td>
              </tr>
              <tr>
                <th scope="row">2D0153150</th>
                <td>2019-05-03</td>
                <td>iSziGDRyHA</td>
                <td>1113.81</td>
                <td>MXN</td>
                <td>GRUPO ELEKTRA SAB DE CV</td>
                <td><b>Servicios</b></td>
              </tr>
              <tr>
                <th scope="row">5D0151626</th>
                <td>2019-05-03</td>
                <td>RSomhDBYwd</td>
                <td>219.23</td>
                <td>MXN</td>
                <td>Movistar</td>
                <td><b>Gastos personales</b></td>
              </tr>
            </tbody>
          </table>
          </div>
        </div>
      </div>
    `;
  }
}

window.customElements.define('categorizador-app', CategorizadorApp);
