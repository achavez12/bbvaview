
import '@polymer/polymer/polymer-element.js';

const $_documentContainer = document.createElement('template');
$_documentContainer.innerHTML = `<dom-module id="shared-styles">
  <template>
    <style>
      .card {
        margin: 24px;
        padding: 16px;
        color: #757575;
        border-radius: 5px;
        background-color: #fff;
        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
      }

      .circle {
        display: inline-block;
        width: 64px;
        height: 64px;
        text-align: center;
        color: #555;
        border-radius: 50%;
        background: #ddd;
        font-size: 30px;
        line-height: 64px;
      }

      h1 {
        margin: 16px 0;
        color: #212121;
        font-size: 22px;
      }

      :root {
        --primary-color: #4285f4;
        --success-color: #2692dd;
      }
      a,
      paper-button {
        font-weight: bold;
      }
      a {
        color: var(--primary-color);
      }
      paper-button {
        color: #fff;
      }
      paper-button.primary {
        background: var(--primary-color);
      }
      paper-button.success {
        background: var(--success-color);
      }
      blockquote {
        border-left: 4px solid #eee;
        margin-left: 4px;
        padding-left: 20px;
      }
    </style>
  </template>
</dom-module>`;

document.head.appendChild($_documentContainer.content);
